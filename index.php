<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESN CULS Prague</title>
    <meta property="og:url" content="http://www.esnculs.cz" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="ESN CULS Prague" />
    <meta property="og:description" content="We are students and volunteers, thrilled to meet new international people, who are coming to Czech University of Life Sciences in Prague" />
    <meta property="og:image" content="" />
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="/assets/css/user.css">
    <link rel="stylesheet" href="/assets/css/accordian.css">
    <link rel="stylesheet" href="/assets/bootstrap/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/calendar/css/snippet.css">
    <link rel="shortcut icon" href="/assets/favicon/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/assets/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon-180x180.png" />
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top opaque">
        <div class="container">
            <div class="navbar-header">
                <a style="display:none" class="navbar-brand navbar-link" href="#"><img style="height: 40px; float:left; margin-top: -10px" src="/assets/img/logo.svg" alt="logo" /></a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="#about">About </a></li>
                    <li role="presentation"><a href="#events">Events </a></li>
                    <li role="presentation"><a href="#team">Our Team </a></li>
                    <li role="presentation"><a href="#partners">Partners </a></li>
                 <!--   <li role="presentation"><a target="_blank" href="http://info.esnculs.cz">Information </a></li> -->
                    <li role="presentation"><a targe="_blank" href="https://docs.google.com/forms/d/1_bi5uqje5EIbzJ_I3y01nvMUO_XPFJYYfIRDDRZlrkk">Join Us </a></li>


                </ul>
            </div>
        </div>
    </nav>
    <div class="jumbotron hero">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-4 get-it">
                    <img id="logo-main" style="height: 230px;" src="/assets/img/logo.svg" alt="logo" />
                    <h1><strong>ESN CULS Prague</strong></h1>
                    <p><strong>International Student Club</strong></p>
                    <p>
                        <a class="btn btn-warning btn-lg" target="_blank" role="button" href="https://my.esnculs.cz/register/international">Apply for your buddy!</a>
                        <a class="btn btn-success btn-lg" role="button" href="#events">Check our activities!</a>
                        <a class="btn btn-primary btn-lg" role="button" target="_blank" href="https://docs.google.com/forms/d/1_bi5uqje5EIbzJ_I3y01nvMUO_XPFJYYfIRDDRZlrkk">Join us!</a>
                    </p>


                </div>


            </div>
        </div>
    </div>


    <section id="about" class="features opaque">
        <div class="container">
            <div class="row mb">
                <div class="col-md-6 mb col-md-offset-2">
                    <h2>ESN CULS Introduction</h2>
                    <p>We are students and volunteers, thrilled to meet new international people, and we are here for everyone coming to the <strong><a href="http://www.czu.cz/en/" style="color: white;" target="_blank" >Czech University of Life Sciences</a></strong>.
                        Besides the University coordinators we are streaming information about CULS, life in the campus and the city of Prague. </p>
                    <p>We help all the incoming students to settle down in the new environment and to meet new friends during many public events we organize. New in Prague? Don't know anyone yet? Want to meet new international friends? Join our events and
                        subscribe to our <strong><a href="https://www.facebook.com/pages/ESN-CULS-Prague/201421116719548" style="color: white;" target="_blank" >Facebook Page</a></strong>!</p>

                </div>
                <div class="col-md-2">
                    <h2><i class="fa fa-child fa-5x mt"></i></h2>
                </div>
            </div>
        </div>
    </section>

    <section id="events" class="about opaque">
        <div class="container">

            <?php include_once( 'calendar/upcoming/upcoming.php') ?>

            <?php include_once( 'calendar/past/past.php') ?>
        </div>
    </section>

    <!--   <section id="join" class="register">
        <div class="container">
            <div class="row register-form">
                <div class="col-md-8 col-md-offset-2">
                    <div style="height: 780px" class="embed-responsive embed-responsive-4by3">
                        <iframe class="embed-responsive-item" src="https://docs.google.com/forms/d/1_bi5uqje5EIbzJ_I3y01nvMUO_XPFJYYfIRDDRZlrkk/viewform?embedded=true#start=embed" width="760" height="780" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
                    </div>

                </div>
            </div>
        </div>
    </section> -->


    <?php 
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    include_once("./team/TeamGen.php"); 
    $teamGen = new TeamGen();
    echo $teamGen->getHtml();
    
    ?>
   

    <section id="partners" class="features opaque ">
        <div class="container">
            <div class="row mb">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>OFFICIAL PARTNERS</h2>
                    <hr class="small">
                    <div class="row mb">
                        <div class="col-md-4">
                            <div class="service-item">
                                <a href="https://www.uniplaces.com" target="_blank"> <img src="assets/img/partners/uniplaces-logo_esnwebsites.png" alt="uniplaces"></a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="service-item">
                                <a href="http://www.vodafone.cz/en/" target="_blank"><img src="assets/img/partners/Vodafone.svg.png" alt="vodafone"></a>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="service-item">
                                <div id="eurolines">
                                    <a href="https://www.elines.cz/en/" target="_blank"><img src="assets/img/partners/Eurolines_logo.svg.png" alt="eurolines"></a>
                                </div>
                            </div>
                        </div>





                    </div>

                    <div class="row mb">
                        <div class="col-md-4">
                            <div class="service-item">
                                <div>
                                    <a href="http://www.freshlabels.cz/" target="_blank"><img src="assets/img/partners/fresh.png" alt="freshlabels"></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="service-item">
                                <div>
                                    <a href="http://bohemianhostels.com/" target="_blank"><img src="assets/img/partners/boho-logo2.png" alt="bohemian hostels"></a>
                                </div>
                            </div>
                        </div>
                      

                    </div>

                    <!-- /.row (nested) -->
                </div>
            </div>
        </div>
    </section>



    <!--section id="testimonials" class="white testimonials">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">What people say, when they see it? </h2>
                <blockquote>
                    <p>I like it, I want it, when can you give it to me?</p>
                    <footer class="quote-footer">President of CzechESN section during My Section presentation</footer>
                </blockquote>
            </div>
        </div>
    </section-->

    </section>
    <footer id="contact" class="foot-padding opaque">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6">
                <img class="img-responsive" alt="esn culs logo" src="/assets/img/logo-wide.svg"></div>
            <div class="col-md-4 col-sm-6 footer-contacts">
                <div><a href="https://www.google.cz/maps/place/ESN+CULS+Prague/@50.130481,14.3774,15z/data=!4m2!3m1!1s0x0:0xec97ac3179ea2b5d?sa=X&amp;ved=0ahUKEwiMlr-x1snJAhVkJHIKHYUlBv0Q_BIIZDAK" target="_blank"><span class="fa fa-map-marker footer-contacts-icon"> </span></a>
                    <p><span class="new-line-span">Kamýcká 1090, 165 00 </span> Prague, Czech Republic</p>
                </div>
                <div><span class="fa fa-envelope footer-contacts-icon"></span>
                    <p> <a href="#" target="_blank">info@esnculs.cz</a></p>
                </div>
                <div><span class="fa fa-facebook footer-contacts-icon"></span>
                    <p> <a href="https://www.facebook.com/ESN-CULS-Prague-201421116719548/?fref=ts" target="_blank">www.facebook.com</a></p>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 footer-about">
                <p class="text-center">Copyright © 2016 ESN CULS Prague </p>
            </div>
        </div>
    </footer>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57158546-2', 'auto');
  ga('send', 'pageview');

</script>
    <script>
        // Scrolls to the selected menu item on the page

        $(function() {
            $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: (target.offset().top - 45)
                        }, 1000);
                        return false;
                    }
                }
            });
            $(window).scroll(function() {
                if ($(window).scrollTop() >= ($('#logo-main').offset().top) + 230) {
                    $('.navbar-brand').fadeIn();
                }
                else {
                    $('.navbar-brand').fadeOut();
                }
            });
        });
    </script>
</body>

</html>
