<?php
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Yaml\Parser;


class TeamGen
{
 
    public $photoDir = "assets/img/board";
    
    public function getHTML(){
        $members = $this->parseMembers();
       
        $html ='
         <section id="team" class="team about opaque">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="col-lg-12">
                        <h2 class="description">OUR TEAM</h2>
        ';
        
        $i = 0;
        foreach ($members as $position => $member){
            if (($i % 4) == 0){
                if ($i!=0){
                    $html.='</div>';
                }
                $html.= '<div class="row pt-md">';
            }
                if (isset($member['photo'])){
                    $photo = $this->photoDir.'/'.$member['photo'];
             
                    if (!file_exists($photo)){
                        $photo = $this->photoDir.'/avatar.jpg';
                    }
                }
                else {
                    $photo = $this->photoDir.'/avatar.jpg';
                }
                
                
                $html.=$this->getContent($member['name'],$position, $member['email'], $photo, $member['social']);
                $i++;
            
        }
        
        $html.='
        
        
                    </div>
                </div>
            </div>
        </div>
    </section>
    ';
        
        return $html;
        
    }
    
    protected function parseMembers(){
        $yaml = new Parser();
        $value = $yaml->parse(file_get_contents('team/team.yaml'));
        return $value;
    }
    
    
    
    
    protected function getContent($name,$position,$email, $photo, $socials){
            $html = '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile ">
                                <div class="img-box">
                                    <img src="'.$photo.'" class="img-responsive">';
            
            $html.= $this->socialMedia($socials);
        
            
                $html.='    </div>
                                <h1>'.$name.'</h1>
                                <h2>'.$position.'</h2>
                                <p>'.$email.'</p>
                            </div>';
                            
            return $html;
                            
        
    }
    
    protected function socialMedia($socials){
        
        if (is_array($socials)){
            
            $html = '<ul class="text-center">';
        
            foreach($socials as $social => $url){
                $html.=                     '<a target="_blank" href="'.$url.'">
                                                <li><i class="fa fa-'.$social.'"></i></li>
                                            </a>';
            }
            $html.=   ' </ul>';
         
        }
        else {
            $html = ' ';
        }
            
        
         
        return $html;
    }
    
}