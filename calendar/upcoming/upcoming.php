<?php

$fb_page_id = "201421116719548";

// get events for the next x years
$year_range = 1;

// automatically adjust date range
// human readable years
$since_date = date('Y-m-d');
$until_date = date('Y-12-31', strtotime('+' . $year_range . ' years'));

// unix timestamp years
$since_unix_timestamp = strtotime($since_date);
$until_unix_timestamp = strtotime($until_date);

$access_token = "680577865404271|f125f8077f2a2a0ff6c43fb2b4814e63";

$fields = "id,name,description,place,timezone,start_time,cover";

$json_link = "https://graph.facebook.com/{$fb_page_id}/events/attending/?fields={$fields}&access_token={$access_token}&since={$since_unix_timestamp}&until={$until_unix_timestamp}";

$json = file_get_contents($json_link);
$json = str_replace('&', '&amp;', $json);
$obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
echo '<div class="panel-group" id="accordion">';

// count the number of events
$event_count = count($obj['data']);

echo '<div class="row">
			<div class="[ col-xs-12 col-sm-offset-2 col-sm-8 ]">
			<h2>Upcoming Events</h2>
				<ul class="event-list">';

for ($x = $event_count-1; $x >= 0; $x--) {
    $start_date = date('Y-m-d', strtotime($obj['data'][$x]['start_time']));

// in my case, I had to subtract 9 hours to sync the time set in facebook
    $start_time = date('H:i', strtotime($obj['data'][$x]['start_time']) - 60 * 60 * 9);

    $start_day = date('d', strtotime($obj['data'][$x]['start_time']));
    $start_month = date('M', strtotime($obj['data'][$x]['start_time']));
    $start_year = date('Y', strtotime($obj['data'][$x]['start_time']));
    
    $pic_big = isset($obj['data'][$x]['cover']['source']) ? $obj['data'][$x]['cover']['source'] : "https://graph.facebook.com/{$fb_page_id}/picture?type=large";

    $eid = $obj['data'][$x]['id'];
    $name = $obj['data'][$x]['name'];
    $description = isset($obj['data'][$x]['description']) ? $obj['data'][$x]['description'] : "";

// place
    $place_name = isset($obj['data'][$x]['place']['name']) ? $obj['data'][$x]['place']['name'] : "";
    $city = isset($obj['data'][$x]['place']['location']['city']) ? $obj['data'][$x]['place']['location']['city'] : "";
    $country = isset($obj['data'][$x]['place']['location']['country']) ? $obj['data'][$x]['place']['location']['country'] : "";
    $zip = isset($obj['data'][$x]['place']['location']['zip']) ? $obj['data'][$x]['place']['location']['zip'] : "";

    $location = "";

    if ($place_name && $city && $country && $zip) {
        $location = "{$place_name}, {$city}, {$country}, {$zip}";
    } else {
        $location = "Location not set or event data is too old.";
    }
	

	
	echo '
	
	<li>
						<time datetime="'.$start_date.' '.$start_time.'">
							<span class="day">'.$start_day.'</span>
							<span class="month">'.$start_month.'</span>
							<span class="year">'.$start_year.'</span>
							<span class="time">'.$start_time.'</span>
						</time>
						<img class="fb-picture" alt="Independence Day" src="'.$pic_big.'" />
						<div class="info">
							<h4>'.$name.'</h4>
							<p class="desc"> </p>
						</div>
						<div class="social">
							<ul>
								<li class="facebook" style="width:33%;"><a href=\'https://www.facebook.com/events/'.$eid.'/\' target="blank"><span class="fa fa-facebook"></span></a></li>
								
							</ul>
						</div>
					</li>';
	
	
	


	
}

echo '	</ul>
			</div>
		</div>
	</div>	
	';

?>